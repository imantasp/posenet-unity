﻿using System.Collections;
using System.Collections.Generic;
using Barracuda;
using UnityEngine;

public class Utils
{
    public static Tensor TransformInput(Texture2D input)
    {
//        Color32[] pixels = input.GetPixels32();
//		
//        float[] array = new float[input.height * input.width * 3];
//		
//
//        unsafe
//        {
//            fixed (float* dst = array)
//            {
//                int pixel = 0;
//                int rowLength = 3 * input.width;
//                int dstOffset = array.Length - rowLength;
//
//				
//                // NN expects rows in opposite order than Unity texture stores them
//                for (var y = 0; y < input.height; y++)
//                {
//                    for (var x = 0; x < input.width; x++)
//                    {
//                        dst[dstOffset + 3 * x + 0] = 2 * pixels[pixel].r / 255.0f - 1.0f;
//                        dst[dstOffset + 3 * x + 1] = 2 * pixels[pixel].g / 255.0f - 1.0f;
//                        dst[dstOffset + 3 * x + 2] = 2 * pixels[pixel].b / 255.0f - 1.0f;
//
//                        pixel++;
//
//                    }
//                    dstOffset -= rowLength;
//                }
//            }
//        }
//		
//        Tensor res = new Tensor(1, input.height, input.width, 3, array);
//		
//        return res;
        
        var x = new Tensor(input);
        //new ReferenceComputeOps(ComputeShaderSingleton.Instance.referenceKernels).Pin(x);
		return x;
    }
    
    public static Texture Scaled(Texture src, int width, int height, Texture dst = null, bool flipHorizontally = false, 
        bool flipVertically = false, FilterMode mode = FilterMode.Trilinear)
    {
        //Debug.LogWarning($"downscale {src.width} x {src.height} => {dst?.width} x {dst?.height}");

        float r = Mathf.Min(src.width, src.height) / (float)Mathf.Max(src.width, src.height);
        float yr = 0f, xr = 0f;

        if (src.width > src.height)
            xr = (1.0f - r) / 2.0f;
        else
            yr = (1.0f - r) / 2.0f;

		
        Rect texR = new Rect(0, 0, width, height);
        _gpu_scale(src, width, height, mode, dst as RenderTexture, xr, yr, flipHorizontally, flipVertically);

        if (dst == null)
        {
            //Get rendered data back to a new texture
            dst = new Texture2D(width, height, TextureFormat.ARGB32, false);
            (dst as Texture2D).Resize(width, height);
        }

        (dst as Texture2D)?.ReadPixels(texR, 0, 0);
        (dst as Texture2D)?.Apply();

        return dst;                 
    }
    static void _gpu_scale(Texture src, int width, int height, FilterMode fmode, 
        RenderTexture dst = null, float xr = 0f, float yr = 0f, bool flipHorizontally = false,
        bool flipVertically = false)
    {
        float hr = flipHorizontally ? -1f : 1f;
        float vr = flipVertically ? -1f : 1f;
		
        //We need the source texture in VRAM because we render with it
        src.filterMode = fmode;
        (src as Texture2D)?.Apply(true);       
						
        //Using RTT for best quality and performance. Thanks, Unity 5
        RenderTexture rtt = dst;
        if (rtt == null)
            rtt = RenderTexture.GetTemporary(width, height, 32);
		
        //Set the RTT in order to render to it
        Graphics.SetRenderTarget(rtt);
		
        //Setup 2D matrix in range 0..1, so nobody needs to care about sized
        GL.LoadPixelMatrix(0,1,1,0);
		
        //Then clear & draw the texture to fill the entire RTT.
        GL.Clear(true,true,new Color(0,0,0,0));
        Graphics.DrawTexture(new Rect(0,0,1,1),src, new Rect(hr*xr, vr*yr, hr*(1.0f - 2*xr), vr*(1.0f - 2*yr)), 0, 0, 0, 0);
    }
}
