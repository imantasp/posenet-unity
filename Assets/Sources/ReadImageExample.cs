﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using Barracuda;

public class ReadImageExample : MonoBehaviour {
	int ImageSize= 513;

    PoseNet posenet = new PoseNet();
    PoseNet.Pose[] poses;

    public GameObject glgo;
    private GLRenderer gl;

	private Model model;
	private IWorker worker;

    void Start ()
    {
	    model = LoadModel();
	    worker = BarracudaWorkerFactory.CreateWorker(BarracudaWorkerFactory.Type.ComputePrecompiled, model);
	    
        Texture2D image = Resources.Load("lifting") as Texture2D;
        image = (Texture2D)Utils.Scaled(image, ImageSize, ImageSize);
        var input = Utils.TransformInput(image);

		worker.Execute(input);

	    var heatmap = worker.Fetch("heatmapsigmoid");
	    //var heatmap = worker.Fetch("heatmap");
	    var offsets = worker.Fetch("offset_2");
	    var displacementsFwd = worker.Fetch("displacement_fwd_2");
	    var displacementsBwd = worker.Fetch("displacement_bwd_2");

		//Debug.Log(mean(heatmap));

        poses = posenet.DecodeMultiplePoses(
            heatmap, offsets,
            displacementsFwd,
            displacementsBwd,
            outputStride: 16, maxPoseDetections: 15,
            scoreThreshold: 0.5f, nmsRadius: 20);

        gl = glgo.GetComponent<GLRenderer>();
	    
	    input.Dispose();
	    worker.Dispose();
	    
	    //Debug.Log(">>> " + poses.Length);
    }

	public static Model LoadModel()
	{
		TextAsset graphModel = Resources.Load ("posenet.bc") as TextAsset;
		var model = ModelLoader.Load(graphModel.bytes);
	    
		// Add sigmoid activation for heatmap
		Layer layer         = new Layer();
		layer.type          = Layer.Type.Activation;
		layer.activation    = Layer.Activation.Sigmoid;
		layer.name          = "heatmapsigmoid";
		layer.datasets = new Layer.DataSet[0];
		layer.weights = new float[0];
	    

		// connect inputs
		layer.inputs = new [] {"heatmap"};

		var newLayers = new Layer[] { layer };
		model.layers = model.layers.Concat(newLayers).ToArray();
		
		// Preprocessing layer 
		float bias = -1.0f;
		float scale = 2.0f;

		layer         		= new Layer();
		layer.type          = Layer.Type.ScaleBias;
		layer.activation    = Layer.Activation.None;
		layer.name          = "mobilenet-prep-input";
		layer.datasets		= new Layer.DataSet[2];

		layer.weights = new float[] { scale, scale, scale, bias, bias, bias };

		var S = new TensorShape(1, 1, 1, 3);
		var B = new TensorShape(1, 1, 1, 3);

		layer.datasets[0].shape = S;
		layer.datasets[0].offset = 0;
		layer.datasets[0].length = S.length;

		layer.datasets[1].shape = B;
		layer.datasets[1].offset = S.length;
		layer.datasets[1].length = B.length;

		// connect inputs
//		layer.inputs = model.layers[1].inputs;
//		model.layers[1].inputs = new [] { layer.name };
//
//		newLayers = new Layer[] { layer };
//		model.layers = newLayers.Concat(model.layers).ToArray();
		

		return model;
	}

    public void OnRenderObject()
    {
        gl.DrawResults(poses, 1f/ImageSize);
    }
    
    // Update is called once per frame
    void Update () {
		
	}

	

	
}
