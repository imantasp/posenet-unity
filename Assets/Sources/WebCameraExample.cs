﻿using System.Collections;
using System.Collections.Generic;
using Barracuda;
using UnityEngine;
using UnityEngine.UI;

public class WebCameraExample : MonoBehaviour {

    public int Width = 513;
    public int Height = 513;
    public int FPS = 30;
	// how many frames to skip after launching inference (provides better alignment between CPU and GPU)
	public int inferenceSkipFrameCount = 3;
	public bool showFPSCounter = true;
	public float scoreThreshold = 0.6f;
    WebCamTexture webcamTexture;
    GLRenderer gl;
    public int ImageSize = 513;
    PoseNet posenet = new PoseNet();
    PoseNet.Pose[] poses;

    private Model model;
    private IWorker worker;
    private Texture2D inferenceTexture;
    private Texture displayTexture;

    public Text fpsCounter;
    
    bool isPosing;
    private Tensor input;
	private int minDisplaySize;

    // Use this for initialization
    void Start ()
    {
	    Application.targetFrameRate = 60;
        minDisplaySize = Mathf.Min(Width, Height);
        
        inferenceTexture = new Texture2D(ImageSize, ImageSize, TextureFormat.ARGB32, false);
        displayTexture = new RenderTexture(minDisplaySize, minDisplaySize, 32);
        GetComponent<Renderer>().material.mainTexture = displayTexture;
        
        WebCamDevice[] devices = WebCamTexture.devices;
        webcamTexture = new WebCamTexture(devices[0].name, Width, Height, FPS);
        
        webcamTexture.Play();

        model = ReadImageExample.LoadModel();
        worker = BarracudaWorkerFactory.CreateWorker(BarracudaWorkerFactory.Type.ComputePrecompiled, model);
        
        gl = GameObject.Find("GLRender").GetComponent<GLRenderer>();

		fpsCounter.gameObject.SetActive(showFPSCounter);
        
        Debug.Log($"Compute support: {SystemInfo.supportsComputeShaders}");

    }
	
	// Update is called once per frame
	void Update ()
	{
        if (showFPSCounter && fpsCounter)
	        fpsCounter.text = $"FPS: {(1f/Time.smoothDeltaTime):00.#}";

		if (webcamTexture.width < 32 || webcamTexture.height < 32)
			return;
		
		//Debug.Log($"cam: angle={webcamTexture.width}, vert={webcamTexture.height}");
	    
		Utils.Scaled(webcamTexture, minDisplaySize, minDisplaySize, displayTexture, 
									webcamTexture.videoRotationAngle == 180,
									webcamTexture.videoRotationAngle == 180 && !webcamTexture.videoVerticallyMirrored);
	    
        if (isPosing) return;
	    
	    //Debug.LogWarning($"=> {webcamTexture.videoRotationAngle} + {webcamTexture.videoVerticallyMirrored}");
	    
	    Utils.Scaled(webcamTexture, ImageSize, ImageSize, inferenceTexture, 
									webcamTexture.videoRotationAngle == 180,
									webcamTexture.videoRotationAngle == 180 && !webcamTexture.videoVerticallyMirrored);
	    
        isPosing = true;
        StartCoroutine(PoseUpdate(inferenceTexture));
    }

    IEnumerator PoseUpdate(Texture2D texture)
    {

        input = Utils.TransformInput(texture);

	    for (int i = 0; i < inferenceSkipFrameCount; i++)
			yield return null;
        
        worker.Execute(input);

	    for (int i = 0; i < inferenceSkipFrameCount; i++)
			yield return null;
        
        var heatmap = worker.Peek("heatmapsigmoid");
        var offsets = worker.Peek("offset_2");
        var displacementsFwd = worker.Peek("displacement_fwd_2");
        var displacementsBwd = worker.Peek("displacement_bwd_2");

        
       // Debug.Log(PoseNet.mean(heatmap));

        poses = posenet.DecodeMultiplePoses(
            heatmap, offsets,
            displacementsFwd,
            displacementsBwd,
            outputStride: 16, maxPoseDetections: 15,
            scoreThreshold: scoreThreshold, nmsRadius: 20);

        isPosing = false;
        
//        displacementsBwd.Dispose();
//        displacementsFwd.Dispose();
//        offsets.Dispose();
//        heatmap.Dispose();
        
        input.Dispose();
    }

    public void OnRenderObject()
    {
        //Debug.Log(poses);
        gl.DrawResults(poses, 1f/ImageSize);
    }

    private void OnDestroy()
    {
        input?.Dispose();
        worker?.Dispose();
    }
}
