﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Barracuda;

public class ByteArrayExample : MonoBehaviour {
    PoseNet posenet = new PoseNet();
    PoseNet.Pose[] poses;

    public GameObject glgo;
    private GLRenderer gl;

    // Use this for initialization
    void Start () {
		
		var heatmap = GetResourcesToTensor("heatmaps",33,33,17);
		var offsets = GetResourcesToTensor("offsets",33,33,34);
		var displacementsFwd = GetResourcesToTensor("displacementsFwd",33,33,32);
		var displacementsBwd = GetResourcesToTensor("displacementsBwd",33,33,32);

        poses = posenet.DecodeMultiplePoses(
            heatmap, offsets,
            displacementsFwd,
            displacementsBwd,
            outputStride: 16, maxPoseDetections: 15,
            scoreThreshold: 0.5f, nmsRadius: 20);

        gl = glgo.GetComponent<GLRenderer>();

    }

	public static Tensor GetResourcesToTensor(string name, int y,int x,int z) {
		TextAsset binary = Resources.Load (name) as TextAsset;
		var floatValues = GetByteToFloat(binary.bytes);
		var tensor = GetFloatToTensor(floatValues, y, x, z);
		return tensor;
	}

	public static Tensor GetFloatToTensor(float[] floatValues, int y,int x,int z) {
		return new Tensor(1, y, x, z, floatValues);
	}

	public static float[] GetByteToFloat(byte[] byteArray) {
		var floatArray = new float[byteArray.Length / 4];
		Buffer.BlockCopy(byteArray, 0, floatArray, 0, byteArray.Length);
		return floatArray;
	}
	
	// Update is called once per frame
	void Update () {

    }
    public void OnRenderObject()
    {
        gl.DrawResults(poses);
    }
}
